# -*- coding: UTF-8 -*-
import core.game as game
import script.base_lib as lib
import script.gui_lib as gui
import script.era_lib as era
import script.mainflow as mf

reg = {
    'index': 0,
    'name': 'Core',
    '名称': '核心'
}


def get_reg():
    return reg


# def profile():
#     gui.goto(profile_list)


def profile_list():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    #
    game.pline('--')
    game.pcmd('  [01]{}'.format(era.get_player()['姓名']),
              1, gui.goto, (era.show_profile, era.get_player()['hash']))
    game.p(' ' * (8-len(era.get_player()['姓名'])*2))
    game.pline('--')
    i = 2
    for each in game.data['人物库']:
        if each == era.get_player():
            continue
        game.pcmd('  [{:0>2}}]{}'.format(i, each['姓名']),
                  i, era.show_profile, each['hash'])
        game.p(' ' * (8-len(era.get_player()['姓名'])*2))
        game.p('体力：')
        game.p(lib.value_bar(era.get_player()[
               '体力'], era.get_player()['体力上限'], 8))
        # game.pl()
        game.p(' 耐力：')
        game.p(lib.value_bar(era.get_player()[
               '耐力'], era.get_player()['耐力上限'], 8))
        # game.pl()
        game.p(' 精力：')
        game.p(lib.value_bar(era.get_player()[
               '精力'], era.get_player()['精力上限'], 8))
        i += 1
    game.pl()
    game.pcmd('返回', game.get_unused_cmd_num(), gui.back)


def rest():
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    game.pwait('你选择休息')
    import script.time_lib as tlib
    tlib.tick()


def chat():
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    game.p('123')


def save():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.data['项目库'] = []
    lib.save_func(mf.into_main)


def load():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    lib.load_func(mf.into_main, mf.into_main)


def config():
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    game.p('123')


def get_item_list():
    item_list = []

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'profile',
        '名称': '人物档案',
        'func': profile_list
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'rest',
        '名称': '休息',
        'func': rest
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'chat',
        '名称': '和神秘人聊天',
        'func': chat
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'save',
        '名称': '保存',
        'func': save
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'load',
        '名称': '读取',
        'func': load
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'config',
        '名称': '设置',
        'func': config
    }
    item_list.append(item)

    return item_list
