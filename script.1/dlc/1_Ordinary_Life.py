# -*- coding: UTF-8 -*-
import random
import core.game as game
import script.base_lib as lib
import script.era_lib as era
import script.gui_lib as gui
import script.mainflow as mf
import script.hero_lib as hero
import script.world_lib as world

reg = {
    'index': 1,
    'name': 'Ordinary Life',
    '名称': '日常生活'
}


def get_reg():
    return reg


def exploration():
    def 战斗():
        # 遭遇
        game.data['世界']['社会']['队伍'].append(era.get_player()['hash'])
        game.data['世界']['社会']['对方'].append(hero.get_enemy(1)['hash'])
        for enemy in game.data['世界']['社会']['对方']:
            game.pl('一个{}出现了。'.format(hero.get_hero(enemy)['特征']['种族'][0]))
        game.plwait()
        # 确定战斗顺序
        priority_list = []
        priority_list.extend(game.data['世界']['社会']['队伍'])
        priority_list.extend(game.data['世界']['社会']['对方'])
        priority_list.sort(key=lambda man: hero.get_hero(man)['属性']['敏捷'])
        loop = True
        while loop:
            for each in priority_list:
                if each in game.data['世界']['社会']['对方']:
                    target = random.choice(game.data['世界']['社会']['队伍'])
                elif each in game.data['世界']['社会']['队伍']:
                    target = random.choice(game.data['世界']['社会']['对方'])
                # 人物依次使用随机技能
                skill_list = []
                for act in era.get_item('动作', '战斗'):
                    if act['可行'](each, target):
                        skill_list.append(act)
                skill = random.choice(skill_list)
                # 计算伤害与效果
                skill['释放'](each, target)
                # 判定胜负
                enemy_alive = 0
                for enemy in game.data['世界']['社会']['对方']:
                    if hero.get_hero(enemy)['状态']['体力'] > 0:
                        enemy_alive += 1
                troop_alive = 0
                for troop in game.data['世界']['社会']['队伍']:
                    if hero.get_hero(troop)['状态']['体力'] > 0:
                        troop_alive += 1
                if enemy_alive == 0:
                    loop = False
                    game.pl('敌人全灭')
                    break
                if troop_alive == 0:
                    loop = False
                    game.pl('我方全灭')
                    return '失败'
        game.data['世界']['社会']['队伍'] = []
        game.data['世界']['社会']['对方'] = []

    def 寻宝():
        # 遭遇
        # 计算触发陷阱
        # 计算反应
        # 搜刮战利品
        pass
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.plwait('你开始了探险。')
    while True:
        # f = random.choice([战斗, 寻宝])
        f = random.choice([战斗])

        if f() == '失败':
            gui.back()

    # game.pline('--')
    # # 生成商店名单
    # market_list = []
    # for each in game.data['项目库']:
    #     if each['type'] == '商店' and each['parent'] == '市场':
    #         market_list.append(each)
    # for i, each in enumerate(era.sort_list_by_name(market_list, '市场')):
    #     text = '[{:0>3}]'.format(i+1)
    #     text += each['名称']
    #     text += ' ' * (6-len(each['名称']))*2
    #     game.pcmd(text, i+1, each['func'])
    #     if i % 5 == 4:
    #         game.pl()
    # text = '[{:0>3}]'.format(i+2)
    # text += '返回'
    # text += ' ' * (6-len('返回'))*2
    # game.pcmd(text, i+1, gui.back)


def market():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到市场，街上的人熙熙攘攘。')
    game.pline('--')
    # 生成商店名单
    market_list = []
    for each in game.data['项目库']:
        if each['type'] == '商店' and each['parent'] == '市场':
            market_list.append(each)
    for i, each in enumerate(era.sort_list_by_name(market_list, '市场')):
        text = '[{:0>3}]'.format(i+1)
        text += each['名称']
        text += ' ' * (6-len(each['名称']))*2
        game.pcmd(text, i+1, gui.goto, each['func'])
        if i % 5 == 4:
            game.pl()
    text = '[{:0>3}]'.format(i+2)
    text += '返回'
    text += ' ' * (6-len('返回'))*2
    game.pcmd(text, i+2, gui.back)


def equipment():
    pass


def weapon_shop():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到武器商店，老板冷漠地没理你。')
    game.pline('--')
    era.show_shop('武器')


def defence_shop():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到防具商店，老板冷漠地没理你。')
    game.pline('--')
    era.show_shop('防具')


def accessory_shop():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到饰品商店，老板冷漠地没理你。')
    game.pline('--')
    era.show_shop('饰品')


def medicine_shop():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到药品商店，老板冷漠地没理你。')
    game.pline('--')
    era.show_shop('药品')


def item_shop():
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    game.pl('你来到道具商店，老板冷漠地没理你。')
    game.pline('--')
    era.show_shop('道具')


def get_item_list():
    item_list = []

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'exploration',
        '名称': '外出探险',
        'func': exploration
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'market',
        '名称': '市场',
        'func': market
    }
    item_list.append(item)

    item = {
        'type': '功能',
        'parent': 'main',
        'name': 'equipment',
        '名称': '装备管理',
        'func': equipment
    }
    item_list.append(item)

    item = {
        'type': '商店',
        'parent': '市场',
        'name': 'weapon_shop',
        '名称': '武器商店',
        'func': weapon_shop
    }
    item_list.append(item)

    item = {
        'type': '商店',
        'parent': '市场',
        'name': 'defence_shop',
        '名称': '防具商店',
        'func': defence_shop
    }
    item_list.append(item)

    item = {
        'type': '商店',
        'parent': '市场',
        'name': 'accessory_shop',
        '名称': '饰品商店',
        'func': accessory_shop
    }
    item_list.append(item)

    item = {
        'type': '商店',
        'parent': '市场',
        'name': 'medicine_shop',
        '名称': '药店',
        'func': medicine_shop
    }
    item_list.append(item)

    item = {
        'type': '商店',
        'parent': '市场',
        'name': 'item_shop',
        '名称': '道具商店',
        'func': item_shop
    }
    item_list.append(item)

    return item_list
