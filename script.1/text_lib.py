# -*- coding: UTF-8 -*-
import core.game as game
import script.base_lib as base


def init():
    game.data['界面列表'] = []


def goto(gui_func):
    game.data['界面列表'].append(gui_func)
    gui_func()


def repeat():
    game.data['界面列表'][-1]()


def back():
    game.data['界面列表'].pop()
    repeat()


def clear(gui_func=None):
    if gui_func:
        game.data['界面列表'] = [gui_func]
    else:
        game.data['界面列表'] = []
