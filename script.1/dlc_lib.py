# -*- coding: UTF-8 -*-
import os
import glob
import importlib
import core.game as game
import script.base_lib as lib
import script.era_lib as era
import script.gui_lib as gui
import script.item_lib as ilib


def init():
    # DLC状态储存了dlc的基本信息和当前状态
    if not 'DLC状态' in game.data:
        game.data['DLC状态'] = []
    for each in glob.glob('script\dlc\*.py'):
        text = 'script.dlc.'+os.path.basename(each).split('.')[0]
        new_module = importlib.import_module(text)
        game.data['DLC状态'].append([new_module.get_reg(), True])


def dlc_gui():
    def change_status(index):
        for each in game.data['DLC状态']:
            if each[0]['index'] == index:
                each[1] = not each[1]
        game.clr_screen()  # 清除屏幕
        game.clr_cmd(clr_default_flow=False)  # 清除选项
        gui.repeat()

    def finish():
        game.clr_screen()
        game.clr_cmd(clr_default_flow=False)  # 清除选项
        gui.back()
    game.clr_screen()  # 清除屏幕
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    for each in game.data['DLC状态']:
        if each[0]['index'] == 0:
            continue
        text = 'DLC-{}: {}：{}'
        text = text.format(each[0]['index'], each[0]['名称'],
                           '开启' if each[1] else '关闭')
        game.pcmd(text, each[0]['index'], change_status, each[0]['index'])
        game.pl()
    game.pcmd('确定', game.get_unused_cmd_num(), finish)


def load():
    for each in glob.glob('script\dlc\*.py'):
        text = 'script.dlc.'+os.path.basename(each).split('.')[0]
        module = importlib.import_module(text)
        reg = module.get_reg()
        if reg['index'] == 0:
            ilib.load_pkg(module)
            continue
        for each in game.data['DLC状态']:
            if each[0]['index'] == reg['index'] and each[1]:
                ilib.load_pkg(module)


def load_item(item):
    game.data['项目库'].append(item)
