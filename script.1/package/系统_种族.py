# -*- coding: UTF-8 -*-

reg = {
    'enabled': True,
    'name': 'species',
    '名称': '种族'
}


def get_item_list():
    item_list = []

    item = {
        'type': '种族',
        'parent': '',
        'name': 'human',
        '名称': '人类',
        '唯一': True,
    }
    item_list.append(item)

    return item_list
