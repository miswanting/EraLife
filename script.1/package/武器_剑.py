# -*- coding: UTF-8 -*-
import core.game as game
import script.base_lib as lib
import script.era_lib as era

reg = {
    'enabled': True,
    'name': 'sword',
    '名称': '剑'
}


def get_item_list():
    item_list = []

    item = {
        'type': '武器',
        'parent': '近战武器',
        'name': 'sword',
        '名称': '剑',
        '唯一': True,
        'func': None,
        '攻击力': 5,
        '价格': 100,
        '部位': ['左手', '右手'],
    }
    item_list.append(item)

    return item_list
