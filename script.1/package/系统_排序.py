# -*- coding: UTF-8 -*-
import core.game as game
import script.hero_lib as hero
import script.gui_lib as gui
import script.mainflow as mf


def get_item_list():
    item_list = []

    item = {
        'type': '排序',
        'parent': '',
        'name': 'create_hero_method',
        '名称': '角色创建方式',
        '排序': [
            'default'
        ],
    }
    item_list.append(item)

    item = {
        'type': '排序',
        'parent': '',
        'name': 'main',
        '名称': '主界面',
        '排序': [
            'exploration',
            'market',
            'profile',
            'profile',
            'rest',
            'chat',
            'save',
            'load',
            'config',
        ],
    }
    item_list.append(item)

    item = {
        'type': '排序',
        'parent': '',
        'name': 'market',
        '名称': '市场',
        '排序': [
            'weapon_shop',
            'defence_shop',
            'accessory_shop',
            'medicine_shop',
            'item_shop'
        ],
    }
    item_list.append(item)

    return item_list
