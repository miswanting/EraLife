import random
import erajs.api as a
from . import 姓名 as xm


class WorldManager():
    """世界由两部分组成，环境与生物
    """

    def __init__(self):
        self.pool = []

    def init(self):
        pass

    def new_world(self):
        """生成世界"""
        world = World()
        world.generate_map()
        self.pool.append(world)
        return world

    def save(self):
        pass

    def move_in(self):
        pass

    def move_out(self):
        pass

    def load_all_world(self):  # TODO
        """从data加载数据并生成pool"""
        self.pool = []
        for world_hash in a.data['db']['world'].keys():
            world = World()
            world.load(a.data['db']['world'][world_hash])
            self.pool.append(world)

    def get_world(self, hash):  # TODO
        """返回某一世界"""
        for world in self.pool:
            if world.data['hash'] == hash:
                return world

    def find_person(self, hash):  # TODO
        """从世界查找人物"""
        for world in self.pool:
            for y, place_line in enumerate(world.data['map']):
                for x, place in enumerate(place_line):
                    if hash in place['person']:
                        return (world, (x, y), place)

    def find_player(self):  # TODO
        """查找玩家的人物"""
        for world in self.pool:
            for y, place_line in enumerate(world.data['map']):
                for x, place in enumerate(place_line):
                    if a.data['db']['player'] in place['person']:
                        return (world, (x, y), place)

    def find_birth_place(self):
        """查找可出生的地方"""
        birth_place = []
        for world in self.pool:
            for y, place_line in enumerate(world.data['map']):
                for x, place in enumerate(place_line):
                    if a.data['data.地点'][place['type']]['出生区域']:
                        birth_place.append((world, (x, y), place))
        return birth_place

    def add_to_birth_place(self, hash):
        """将人物HASH加入到出生地"""
        tmp = self.find_birth_place()
        tmp = random.choice(tmp)
        tmp[0].data['map'][tmp[1][1]][tmp[1][0]]['person'].append(hash)

    def add_to_place(self, person, world, x, y):
        """将人物加入到地方"""
        world.data['map'][y][x]['person'].append(person.data['hash'])
        world.save()

    def find_all_person_in_place(self, world, x, y):
        """查找地点所拥有的所有人物"""
        return world.data['map'][y][x]['person']


class World():
    def __init__(self):
        self.data = {
            'name': xm.get_random_name(),
            'hash': a.new_hash(),
            'map': {}
        }

    def save(self):
        if not 'world' in a.data['db'].keys():
            a.data['db']['world'] = {}
        a.data['db']['world'][self.data['hash']] = self.data

    def load(self, world_data):
        self.data = world_data

    def generate_map(self):
        """生成世界地图"""
        place_in_map = []  # 在一个地图上含有多少个地点
        for place in a.data['data.地点'].keys():  # 计算每个地点在地图上存在多少个
            count_max = random.randint(
                a.data['data.地点'][place]['min'], a.data['data.地点'][place]['max'])
            for i in range(a.data['data.地点'][place]['min'], count_max+1):
                place_in_map.append(place)
        w = 0  # 地图宽度
        h = 0  # 地图高度
        while w*h < len(place_in_map):  # 优先提升高度
            if w == h:
                h += 1
            else:
                w += 1
        candidate_list = []  # 候选补充区域类型
        for place in a.data['data.地点'].keys():  # 用补充区域来补充地图
            if a.data['data.地点'][place]['补充区域']:
                candidate_list.append(place)
        for i in range(w*h-len(place_in_map)):
            place_in_map.append(random.choice(candidate_list))
        place_in_map = random.sample(place_in_map, len(place_in_map))  # 随机化列表
        tmp = []
        for each in place_in_map:  # 将名单替换成节点
            new_place = {
                'name': xm.get_random_name()+each,  # 地点名称
                'hash': a.new_hash(),
                'type': each,  # 地点类型
                'person': [],  # 地点内部含有的全部任务索引
                'area': {},  # 地点内部含有的功能性区域
            }
            tmp.append(new_place)
        place_in_map = tmp
        game_map = []
        for y in range(h):  # 将一维列表组装为二维列表
            line = []
            for x in range(w):
                line.append(place_in_map.pop(0))
            game_map.append(line)
        self.data['map'] = game_map  # 将数据推入游戏环境
        for y, place_line in enumerate(self.data['map']):
            for x, place in enumerate(place_line):
                # 加入功能管理者
                for func_name in a.data['data.地点'][place['type']]['功能'].keys():
                    if func_name == '郊区':
                        for i in range(random.randint(2, 5)):
                            person = a.data['pm'].new_random_person()
                            person.data['职业'] = '强盗'
                            place['person'].append(person.data['hash'])
                            person.save()
                        place['area'][func_name] = {  # 功能性区域的主管加入方式
                            'admin': person.data['hash']
                        }
                    if func_name == '市场':
                        place['area'][func_name] = {}
                        for shop in a.data['data.others']['市场'].keys():
                            person = a.data['pm'].new_random_person()
                            place['person'].append(person.data['hash'])
                            person.data['职业'] = '商店老板'
                            person.save()
                            place['area'][func_name][shop] = {  # 功能性区域的主管加入方式
                                'admin': person.data['hash']
                            }
                    if func_name == '酒馆':
                        person = a.data['pm'].new_random_person()
                        place['person'].append(person.data['hash'])
                        person.data['职业'] = '酒馆老板'
                        person.save()
                        place['area'][func_name] = {  # 功能性区域的主管加入方式
                            'admin': person.data['hash'],
                            '酒馆上座率': 0.5,
                            '酒馆桌子数目': 4
                        }
                # 加入随机普通人
                for i in range(random.randint(1, 3)):
                    person = a.data['pm'].new_random_person()
                    place['person'].append(person.data['hash'])
                    person.save()
