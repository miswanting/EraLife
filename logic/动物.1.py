import random
import erajs.api as a


class AnimalManager:
    def __init__(self):
        self.data = {
            'child': []
        }

    def save(self):
        data = self.data
        data['child'] = []
        for animal in self.data['child']:
            data['child'].append(animal.save())
        a.data['db']['animal'] = data

    def load(self):
        self.data = a.data['db']['animal']
        self.data['child'] = []
        for animal_data in a.data['db']['animal']['child']:
            animal = Animal()
            animal.load(animal_data)
            self.data['child'].append(animal)

    def new_random_animal(self):
        animal_type = random.choice(list(a.data['data.动物'].keys()))
        animal = Animal(animal_type)
        self.data['child'].append(animal)
        return animal


class Animal:
    def __init__(self, type=None):
        self.data = {
            'hash': a.new_hash()
        }
        if type:
            self.data['type'] = type
            for key in a.data['data.动物'][type]:
                self.data[key] = a.data['data.动物'][type][key]

    def save(self):
        return self.data

    def load(self, data):
        self.data = data

    def is_person(self):
        return False
