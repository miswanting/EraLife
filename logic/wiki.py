import erajs.api as a


class Wiki:
    def init(self):
        pass

    def ui_wiki(self):
        a.page()
        a.h('百科全书')
        a.t()
        a.b('哲学百科', a.goto, self.ui_哲学)
        a.t()
        a.b('种族百科', a.goto, self.ui_种族)
        a.t()
        a.b('特殊人物百科', a.goto, self.ui_特殊人物)
        a.t()
        a.b('职业百科', None, disabled=True)
        a.t()
        a.b('性格百科', None, disabled=True)
        a.t()
        a.b('技能百科', None, disabled=True)
        a.t()
        a.b('魔法百科', None, disabled=True)
        a.t()
        a.b('物品百科', None, disabled=True)
        a.t()
        a.b('书籍百科', a.goto, self.ui_书籍)
        a.t()
        a.b('属性百科', None, disabled=True)
        a.t()
        a.t()
        a.b('返回', a.back)

    def ui_书籍(self):
        def show_书籍(name):
            def next_page():
                nonlocal name, page
                if page+1 < len(a.data['data.书籍'][name]):
                    page += 1
                    a.clear_gui(1)
                    a.goto(show_page)

            def last_page():
                nonlocal name, page
                if page > 0:
                    page -= 1
                    a.clear_gui(1)
                    a.goto(show_page)

            def back():
                a.clear_gui(1)
                a.back()

            def show_page():
                nonlocal name, page
                a.page()
                for line in a.data['data.书籍'][name][page]:
                    a.t('　　{}'.format(line))
                    a.t()
                if page > 0:
                    a.b('上一页', last_page)
                else:
                    a.b('上一页', func=None, disabled=True)
                if page+1 < len(a.data['data.书籍'][name]):
                    a.b('下一页', next_page)
                else:
                    a.b('下一页', func=None, disabled=True)
                a.b('返回', back)
            page = 0
            a.goto(show_page)
        a.page()
        a.h('书籍百科')
        a.t()
        for name in a.data['data.书籍']:
            a.b(name, a.goto, show_书籍, name)
        a.t()
        a.b('返回', a.back)

    def ui_哲学(self):
        def show_哲学(name):
            a.page()
            a.h(name)
            a.t()
            for line in a.data['data.wiki']['哲学'][name]:
                a.t('　　{}'.format(line))
                a.t()
            a.b('返回', a.back)
        a.page()
        a.h('哲学百科')
        a.t()
        for name in a.data['data.wiki']['哲学']:
            a.b(name, a.goto, show_哲学, name)
        a.t()
        a.b('返回', a.back)

    def ui_种族(self):
        def show_种族(name):
            a.page()
            a.h(name)
            a.t()
            for line in a.data['data.种族'][name]['描述']:
                a.t('　　{}'.format(line))
                a.t()
            a.b('返回', a.back)
        a.page()
        a.h('种族百科')
        a.t()
        for name in a.data['data.种族'].keys():
            a.b(name, a.goto, show_种族, name)
        a.t()
        a.b('返回', a.back)

    def ui_特殊人物(self):
        def show_特殊人物(name):
            a.page()
            a.h(name)
            a.t()
            for line in a.data['data.特殊人物'][name]['描述']:
                a.t('　　{}'.format(line))
                a.t()
            a.t()
            a.b('返回', a.back)
        a.page()
        a.h('特殊人物百科')
        a.t()
        for name in a.data['data.特殊人物'].keys():
            a.b(name, a.goto, show_特殊人物, name)
        a.t()
        a.t()
        a.b('返回', a.back)
