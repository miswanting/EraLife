import erajs.api as a


class SkillManager:
    def __init__(self):
        self.data = {
            'child': []
        }
        for name in a.data['data.技能']:
            skill = Skill(name)
            self.data['child'].append(skill)


class Skill:
    def __init__(self, name):
        self.data = {
            'name': name,
            'data': a.data['data.技能'][name]
        }
