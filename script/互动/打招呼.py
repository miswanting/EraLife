import sys
import random


def show():  # 显示可行性
    return True


def active():  # 点击可行性
    return True


def main(sub, obj, act):
    global a
    # 行为描述
    a.page()
    p = a.data['km'].get(sub, obj)
    t = [
        '{sub_系统称呼}亲切地对{obj_name}打招呼。',
        '{sub_系统称呼}对{obj_name}打了招呼。'
    ]
    a.t(random.choice(t).format(**p), True)
    a.t()
    # 输出动作内容
    if '囚禁者' in obj.think(sub)['关系']:
        if -10 < obj.think(sub)['好感'] and obj.think(sub)['好感'] <= 5:
            a.t('{obj_name}冷眼看了你一眼。'.format(**p), True)
            a.t()
        elif 5 < obj.think(sub)['好感']:
            a.t('{obj_name}看了你一眼。'.format(**p), True)
            a.t()
    elif -10 < obj.think(sub)['好感'] <= 5:
        a.t('{obj_name}礼貌的回应。'.format(**p))
        a.t()
    elif 5 < obj.think(sub)['好感']:
        a.t('{obj_name}很开心。'.format(**p))
        a.t()
    # 计算动作效果
    a.t('{obj_name}对{sub_系统称呼}好感+1。'.format(**p), True)
    a.t()
    obj.think(sub)['好感'] += 1
    a.back()


a = sys.argv[0]
a.data['act']['打招呼'] = {
    'show': show,
    'active': active,
    'main': main
}
